# landsat-viewer-api

> Backend services for querying and viewing recent [LANDSAT 8](https://landsat.usgs.gov) scenes.


## Running locally for development

```bash
mvn spring-boot:run
```


## Testing

```bash
mvn test
```
